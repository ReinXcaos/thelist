from django.contrib import admin
from .models import Comprador, Tienda, Productos

# Register your models here.

admin.site.register(Comprador)
admin.site.register(Tienda)

admin.site.register(Productos)