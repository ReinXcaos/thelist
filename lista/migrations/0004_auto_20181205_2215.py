# Generated by Django 2.1.2 on 2018-12-06 01:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lista', '0003_listas_productos'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productos',
            name='lista_aso',
        ),
        migrations.DeleteModel(
            name='Listas',
        ),
    ]
