from django.db import models

# Create your models here.

class Comprador(models.Model):
    correo = models.EmailField(max_length=254)
    nombre = models.CharField(max_length=100)
    run = models.CharField(max_length=100) 
    claveUsu = models.CharField(max_length=40) 
    fechaNac = models.DateField() 
    fono = models.IntegerField()  
    region = models.CharField(max_length=100)
    comuna = models.CharField(max_length=100)
    
    def __str__(self):
        return "ADOPTANTE"

class Tienda(models.Model):
    nombreTienda = models.CharField(max_length=100)
    sucursal = models.CharField(max_length=100)
    direccion = models.CharField(max_length=200)
    region = models.CharField(max_length=100)
    comuna = models.CharField(max_length=100)

    def __str__(self):
        return " Nombre: "+self.nombreTienda+" Sucursal: "+self.sucursal

class Productos(models.Model):
    nombreProd = models.CharField(max_length=100)
    costop = models.IntegerField()
    costor = models.IntegerField()
    Tienda = models.CharField(max_length=100)
    notas = models.CharField(max_length=100)
    comprado = models.BooleanField(default=False)

    def __str__(self): 
        return self.nombreProd
