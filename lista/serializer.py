from .models import Comprador

from rest_framework import serializers

class CompradorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comprador
        fields = ('url','id','correo','nombre','run','claveUsu','fechaNac','fono','region','comuna')
    