from django.urls import path,include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'comprador', views.CompradorViewSet)

urlpatterns = [
    path('',views.index,name="index"),
    path('login',views.login,name="login"),
    path('registro',views.registro,name="registro"),
    path('registro/CrearUsuarios',views.CrearUsuarios,name="CrearUsuarios"),
    path('tienda',views.tienda,name="tienda"),
    path('tienda/CrearTienda',views.CrearTienda,name="CrearTienda"),
    path('editarti',views.editarti,name="editarti"),
    path('editarti/eliminar/<int:id>',views.eliminar,name="eliminar"),

    path('crearlista',views.crearlista,name="crearlista"),
    path('crearlista/CrearProducto',views.CrearProducto,name="CrearProducto"),
    path('crearlista/quitar/<int:id>',views.quitar,name="quitar"),
     
    path('login/iniciar',views.iniciar,name="iniciar"),
    path('login/iniciar',include('social_django.urls', namespace="iniciar")),  
    path('CerrarSesion',views.CerrarSesion,name="CerrarSesion"),

    path('api/',include(router.urls))
]