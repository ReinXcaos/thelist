from django.shortcuts import render, redirect
from .models import Comprador, Tienda, Productos
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout, login as auth_login
from django.views import generic
from django.db.models import Sum

from .serializer import CompradorSerializer
from rest_framework import viewsets
# Create your views here.


def index(request):
    return render(request,'index.html')

def login(request):
    return render(request,'login.html',{})     

def registro(request):
    return render(request,'registro.html',{})

@login_required(login_url='login')
def tienda(request):
    return render(request,'tienda.html')

@login_required(login_url='login')
def crearlista(request):
    count= Productos.objects.all().count()
    return render(request,'crearlista.html',{'sumaPre' : Productos.objects.aggregate(Sum('costop'))['costop__sum'],'sumaTot' : Productos.objects.aggregate(Sum('costor'))['costor__sum'],'count': count,'Productos':Productos.objects.all(),'Tienda':Tienda.objects.all()})

@login_required(login_url='login')
def editarti(request):
    return render(request,'editarTiendas.html',{'Tienda':Tienda.objects.all()})

def CrearUsuarios(request):
    
    correo = request.POST.get('correo','')
    nombre = request.POST.get('nombre','')
    run = request.POST.get('run','')
    claveUsu = request.POST.get('claveUsu','')
    fechaNac = request.POST.get('fechaNac',18)
    fono = request.POST.get('fono','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    comprador = Comprador(correo=correo,nombre=nombre,run=run,claveUsu=claveUsu,fechaNac=fechaNac,fono=fono,region=region,comuna=comuna)
    comprador.save()
    user=User.objects.create_user(username=comprador.correo,password=comprador.claveUsu)
    user.save()
    return redirect('index')

def CrearProducto(request):
    nombreProd = request.POST.get('nombreProd','')
    costop = request.POST.get('costop',1)
    costor = request.POST.get('costor',1)
    Tienda = request.POST.get('Tienda','')
    notas = request.POST.get('notas','')
    comprado  = request.POST.get('comprado',True)
    productos = Productos(nombreProd=nombreProd,costop=costop,costor=costor,Tienda=Tienda,notas=notas,comprado=comprado)
    productos.save()
    return redirect('crearlista')    

def quitar(request,id):
    pro=Productos.objects.get(pk=id)
    pro.delete()
    return redirect('crearlista')

def CrearTienda(request):

    nombreTienda = request.POST.get('nombreTienda','')
    sucursal = request.POST.get('sucursal','')
    direccion = request.POST.get('direccion','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    tienda = Tienda(nombreTienda=nombreTienda,sucursal=sucursal,direccion=direccion,region=region,comuna=comuna)
    tienda.save()
    return redirect('index')

@login_required(login_url='login')
def eliminar(request,id):
    ti=Tienda.objects.get(pk=id)
    ti.delete()
    return redirect('editarti')

def iniciar(request):
    correo = request.POST.get('correo','')
    claveUsu = request.POST.get('claveUsu','')
    user = authenticate(request,username=correo, password=claveUsu)

    if user is not None:
        auth_login(request,user)
        return redirect("index")
    else:
        return redirect("login")

@login_required(login_url='login')
def CerrarSesion(request):
   
    logout(request)
    return redirect('index')

class CompradorViewSet(viewsets.ModelViewSet):
    queryset = Comprador.objects.all()
    serializer_class = CompradorSerializer